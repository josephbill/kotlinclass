//control flows are your if and else , your logical code
//they can be stored in variables
//basically your if and else statements
fun max(a: Int, b: Int): Int {
    var answer =
        if (a > b) {
           return a
        } else if (a == b) {
           return a
        } else {
            return  b
        }

}
//calling function max
//using function main to print out our values
fun main(args: Array<String>){
    var a: Int = 10
    var b: Int = 4
    var max: Int = max(a, b)
    println(max)
}