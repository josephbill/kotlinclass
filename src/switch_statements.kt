//basically switch...case
//uses the keyword when
//used for infinity cases
//in the function below num can be basically anything
fun printNumber(num: Int){
    when (num){
        0 -> println("number is zero")
        1 -> println("number is one")
        else ->{
            println("Number is anything else")
        }
    }
}

fun main(args: Array<String>){
    var a: Int = 0
    printNumber(a)
}

//when was used as a switch case
//we are printing different statements according to the associated value
