//to declare an imutable variable in kotlin use the val keyword
val name_user_imutable: String = "Joseph";

//u cannot re assign imutable variables

//to declare a mutable variable use the var keyword

var name_user_mutable = "Joseph"

fun main(args: Array<String>){
    println(name_user_imutable)
}